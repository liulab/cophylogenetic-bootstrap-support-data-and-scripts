# Cophylogenetic bootstrap support -- data and scripts
Contains Confidence interval estimation for Cophylogenetic reconciliation (CO3) method pipeline in `CO3_scripts` directory. Scripts and data used in paper are found in `Experimental_data_scripts` directory. 

---------------
Bootstrapping cophylogenies with CO^3 
---------------

Sequential CO^3 pipeline is provided as file `CO3_sequential_bootstrapping_cophylogenies_pipeline.sb` in `CO3_scripts` directory.


### Input: 
* host alignment fasta file
* symbiont alignment fasta file
* outputfile
* work directory


Example command: 
```
        ./CO3_sequential_bootstrapping_cophylogenies_pipeline.sb host.fasta symb.fasta CO3 workdir
```

### Output:
* 4 files inside workdir: outputfile_cospeciation.txt, outputfile_switch.txt, outputfile_loss.txt, outputfile_duplication.txt

* Each file contains 3 columns: the % bootstrap support, the host lineage in the event; the symbiont lineage in the event

Example output:
```
        100.0 h5_h9|s1_s2_s12
        90.0 h1_h4_h6_h7_h8_h12_h15|s4_s5_s6_s11_s13_s15
```
from Bio import Phylo
import sys
import re

tree1=sys.argv[1]
out_tree=sys.argv[2]
t = Phylo.read(tree1,"newick")

leaves = t.get_terminals()
for l in leaves:
  if l.name[0].lower() == "x":
    print(l.name)
    try:
      t.prune(l.name)
    except Exception:
      pass

Phylo.write(t,out_tree,"newick")


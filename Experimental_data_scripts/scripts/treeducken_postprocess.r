library(treeducken)
library(geiger)

###filenames ###
full_history<-"full_treeducken_history.txt"
new_event_history<-"true_events_modified_treeducken"
file_host<-"host"
file_symb<-"symb"
host_symb_associations_file<-"links_empress"

### Treeducken
sink(full_history)
cophy_obj <- sim_cophylo_bdp(hbr = lambda_H, hdr = mu_H, sbr = lambda_S, sdr = mu_S, cosp_rate = lambda_C, host_exp_rate = hs_rate, time_to_sim = time, numbsim = 1, host_limit = 0)
sink()

### write model species trees to files ###
write.table(write.tree(cophy_obj[[1]]$host_tree), file_host, append = FALSE, sep = " ", row.names = FALSE, col.names = FALSE, quote=FALSE)
write.table(write.tree(cophy_obj[[1]]$symb_tree), file_symb, append = FALSE, sep = " ", row.names = FALSE, col.names = FALSE, quote=FALSE)

### read in instrumented Treeducken outputs from history file ###
file_content <- readLines(full_history) # Read the contents of the file
last_separator <- max(grep("^-{30,}$", file_content))
lines_to_read <- file_content[(last_separator + 1):length(file_content)]
lines_to_read <- trimws(lines_to_read)
lines_to_read <- lines_to_read[nzchar(lines_to_read)]
df <- read.table(text = lines_to_read, header = TRUE) 
colnames(df) <- c("Event_Type", "Host_Index", "Symbiont_Index", "Event_Time") 

### reformat modified Treeducken's output event history: (event host_node symb_node) ###
relabel_treeducken_event_history <- function(event_history, hosttree, symbtree, default_history){ #where tree is a phylo object
    #host trees
    tot_internal_nodes_h<-hosttree$Nnode # total number of nodes
    num_leaf_host<-length(hosttree$tip.label)
    start_internal_nodes_h<-num_leaf_host+1
    end_internal_nodes_h<-start_internal_nodes_h+tot_internal_nodes_h-1
    labels_host<-list()
    for (i in start_internal_nodes_h:end_internal_nodes_h){
        # rename internal nodes to its subtree's leaf nodes separated by "_"
        name<-paste(tips(hosttree,i),collapse = '_') 
        labels_host <- c(labels_host, name)
    }
    hosttree$node.label <- labels_host

    #symb trees
    tot_internal_nodes_s<-symbtree$Nnode # total number of nodes
    num_leaf_symb<-length(symbtree$tip.label)
    start_internal_nodes_s<-num_leaf_symb+1
    end_internal_nodes_s<-start_internal_nodes_s+tot_internal_nodes_s-1
    labels_symb<-list()
    for (i in start_internal_nodes_s:end_internal_nodes_s){
        # rename internal nodes to its subtree's leaf nodes separated by "_"
        name<-paste(tips(symbtree,i),collapse = '_') 
        labels_symb <- c(labels_symb, name)
    }
    symbtree$node.label <- labels_symb
    num_events<-nrow(event_history)
    events<-c()
    hosts<-c()
    symbs<-c()
    prefix_host<-"H" 
    prefix_symb<-"S"

    # mapping to known format event history
    for (i in 1:num_events){
        if (event_history$Event_Type[i] == "I"){
            print("Initialized")
            next
        }
        else{
            events <- c(events, event_history$Event_Type[i])
        }
    }
    # default treeducken event history needs relabeling of host & symbiont node names
    for (i in 1:num_events){
        if (event_history$Event_Type[i] == "I"){
            print("Initialized")
            next
        }
        else{
            if (substr(event_history$Host_Index[i], 1, 1) == "H" | substr(event_history$Host_Index[i], 1, 1) == "X" ){
                hosts <- c(hosts,event_history$Host_Index[i])
            }
            else if (as.integer(event_history$Host_Index[i]) > num_leaf_host){ #hosts
                hosts <- c(hosts, labels_host[as.integer(event_history$Host_Index[i])-num_leaf_host])
            }
            else{
                hosts <- c(hosts, paste0(prefix_host,event_history$Host_Index[i]))
            }
            if (substr(event_history$Symbiont_Index[i], 1, 1) == "S" | substr(event_history$Symbiont_Index[i], 1, 1) == "X"){
                symbs <- c(symbs,event_history$Symbiont_Index[i])
            }
            else if (as.integer(event_history$Symbiont_Index[i]) > num_leaf_symb){ #symbs
                symbs <- c(symbs, labels_symb[as.integer(event_history$Symbiont_Index[i])-num_leaf_symb])
            }
            else{
                symbs <- c(symbs, paste0(prefix_symb,event_history$Symbiont_Index[i]))
            }
        }
    }
    new_event_history<-data.frame(events, 
    paste(hosts, sep=" "), data.frame("symbs" = paste(symbs, sep=" ")))
    colnames(new_event_history) <- c('events', 'hosts', 'symbs')
    print(new_event_history)
    return(new_event_history)
}

new_event_history<-relabel_treeducken_event_history(df, cophy_obj[[1]]$host_tree, cophy_obj[[1]]$symb_tree, FALSE)
write.table(new_event_history, file_event_history, append = FALSE, sep = " ", row.names = FALSE, col.names = FALSE, quote=FALSE)
## optionally can also save the event history from unmodified, original Treeducken: ##
#old_event_history<-relabel_treeducken_event_history(event_history(cophy_obj[[1]]), cophy_obj[[1]]$host_tree, cophy_obj[[1]]$symb_tree, TRUE)
#write.table(old_event_history, treeducken_event_history, append = FALSE, sep = " ", row.names = FALSE, col.names = FALSE, quote=FALSE)

### output instrumented Treeducken's final host-symbiont association matrix, by reading the last matrix from history ###
file_content <- readLines(full_history)
all_sep <- grep("^-{30,}$", file_content)
last_sep <- all_sep[length(all_sep)]
secondlast_sep <- all_sep[length(all_sep)-1]
lines_to_read <- file_content[(secondlast_sep+1):(last_sep-1)]
lines_to_read <- trimws(lines_to_read)
lines_to_read <- lines_to_read[nzchar(lines_to_read)]
lines_to_read<- lines_to_read[5:length(lines_to_read)]
lines_to_read <- lines_to_read[nzchar(lines_to_read)]
lines_split <- strsplit(lines_to_read, "\\s+")

# Extracting row and column names
col_names <- unlist(lapply(lines_split[[1]], function(x) gsub("\\[|\\]", "", x)))
row_names <- sapply(lines_split[-1], `[`, 1)

# Extracting data values
data_values <- sapply(lines_split[-1], function(x) as.numeric(x[-1]))

# Creating the dataframe
df <- as.data.frame(data_values)
rownames(df) <- row_names
colnames(df) <- col_names

## output association links in format: (symbion:host)##
Which.names.instrumented <- function(df, value, output_file_links){
    indices <- which(df == 1, arr.ind = TRUE)
    result <- character(length = nrow(indices))
    for (i in seq_len(nrow(indices))) {
        result[i] <- paste0("S", indices[i, "row"], ":", "H", indices[i, "col"])
    }
    links_reformat <- paste(result, collapse = "\n")
    write(links_reformat, output_file_links)
}
Which.names.instrumented(df, 1, host_symb_associations_file) 

import re
from ete3 import Tree
import sys

def ground_truth_event_regex(origin_recon,Phi_type):
    """
    Parse input string into two dictionaries.
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type
    """
    #replace event types
    origin_recon = origin_recon.replace("CSP","Cospeciation")
    origin_recon = origin_recon.replace("SSP","Duplication")
    origin_recon = origin_recon.replace("SX","Loss")
    origin_recon = origin_recon.replace("MTB","Loss")
    origin_recon = origin_recon.replace("SHS","Host_switch")
    origin_recon = origin_recon.replace("SHE","Host_switch")

    temp=origin_recon.split("\n")
    dict_recon = {}
    for i in range(len(temp)):
      line = temp[i].split(" ")
      if len(temp[i]) > 0:
        event = line[0].strip()
        h = line[1].split('_')
        s = line[2].split('_')
        h = [element for element in h if element and element.strip()]
        s = [element for element in s if element and element.strip()]
        h_sort = sorted(h, key=sort_key)
        s_sort = sorted(s, key=sort_key)

        h_join = "_".join(h_sort)
        s_join = "_".join(s_sort)
        if event != event_type:
            continue
        if event == "Host_switch":
          if Phi_type == "event_host_symb":
            this_event = h_join+"|"+s_join+"|"+event
          elif Phi_type == "host_symb":
            this_event = h_join+"|"+s_join
        dict_recon[this_event] = 0
    return dict_recon

def cophy_empress_regex(recon, Phi_type, event_type):
    """
    Parse input string into two dictionaries.
    Output: dict_recon contains rows of events, dict_numevents contains number of each event type
    """
    temp=recon.split()

    dict_recon = {}
    # append to dictionary all events
    for i in range(len(temp)):
      line = temp[i].split(",")
      if len(line) > 1:
        event = line[2]
        h = line[1].strip('_').split('_')
        s = line[0].strip('_').split('_')
        h = [element for element in h if element and element.strip()]
        s = [element for element in s if element and element.strip()]
        h_sort = sorted(h, key=sort_key)
        s_sort = sorted(s, key=sort_key)
        h_join = "_".join(h_sort)
        s_join = "_".join(s_sort)
        if event == "Transfer": # rename Transfer to Host_switch
            event = "Host_switch"
        if event != event_type:
            continue
        if event != "Contemporaneous":
          if Phi_type == "event_host_symb":
            this_event = h_join+"|"+s_join+"|"+event
          elif Phi_type == "host_symb":
            this_event = h_join+"|"+s_join
          dict_recon[this_event] = 0
    return dict_recon

def rename_p0_h0(simulated_recon, hosttree,symbtree):
      host = Tree(hosttree,format=1)
      symb = Tree(symbtree,format=1)
      h_root = host.get_tree_root()
      s_root = symb.get_tree_root()
      h_root_name=""
      s_root_name=""
      for leaf in h_root:
        if len(h_root_name) <= 0:
           h_root_name = leaf.name
        else:
           h_root_name = h_root_name +"_"+ leaf.name
      for leaf in s_root:
        if len(s_root_name) <= 0:
           s_root_name = leaf.name
        else:
           s_root_name = s_root_name +"_"+ leaf.name
      simulated_recon = simulated_recon.replace("_h0",h_root_name)
      simulated_recon = simulated_recon.replace("_p0",s_root_name)
      return simulated_recon

def sort_key(s):
    # Split at the first non-digit character after the initial 'h'
    match = re.search(r'\d+', s)
    numerical = int(match.group()) if match else float('inf')  # Use infinity for non-numeric values
    match = re.search(r'[a-zA-Z]+', s)
    string = match.group() if match else ''
    return (int(numerical), string)

if __name__ == "__main__":
  anno_events = sys.argv[1] #annotation cophylogeny file
  est_events = sys.argv[2]  #bootstrap replicate estimated cophylogenies path + prefix. eg: /mydir/cophy_empress_ and I will completed the string /mydir/cophy_empress_1.csv, etc
  hosttree = sys.argv[3]    #model host species tree with internal nodes labeled
  symbtree = sys.argv[4]    #model symbiont species tree with internal nodes labeled
  bootstrapreps=int(sys.argv[5]) #num bootstrap reps
  outfilepath=sys.argv[6] # outfile
  Phi_type="host_symb" # phi=(s,h)
  event_types = ["Host_switch", "Cospeciation","Duplication","Loss"]
  with open(anno_events,"r") as infile:
      original_recon = infile.read()
  for e in event_types:
      dict_annotate={}
      original_recon = rename_p0_h0(original_recon, hosttree,symbtree)
      dict_annotate = cophy_empress_regex(original_recon,Phi_type, e)    # annotation cophylogeny for event type e
      for i in range(1, bootstrapreps+1):
          try:
              with open(est_events+str(i)+".csv","r") as infile:
                simulated_recon = infile.read()
          except FileNotFoundError:
              print(f"File not found: {est_events + str(i) + '.csv'}")
              continue
      simulated_recon = rename_p0_h0(simulated_recon, hosttree,symbtree)  # bootstrap cophylogeny for event type e
      sim_events = cophy_empress_regex(simulated_recon,Phi_type,e)
      for s in sim_events:
        if s in dict_annotate:
            dict_annotate[s] += 1
      if e == "Cospeciation":
        suffix="_cospeciation.txt"
      elif e == "Host_switch":
        suffix="_switch.txt"
      elif e== "Loss":
        suffix="_loss.txt"
      elif e == "Duplication":
        suffix="_duplication.txt"
      i=0
      outfile=outfilepath+suffix
      with open(outfile, "w+") as outfile:
        for key in dict_annotate:
          outfile.write(str(dict_annotate[key]/(bootstrapreps))+" "+key+"\n")
          i+=1

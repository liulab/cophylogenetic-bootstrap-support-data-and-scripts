from ete3 import Tree, TreeNode
import sys

def main():
    tree=sys.argv[1]
    outfile=sys.argv[2]

    with open(tree, 'r') as infile:
       treestr = infile.read().replace("0.000000","0.000001").replace(":0)",":0.000001)").replace(":0,",":0.000001,").replace("0.00000","0.000001").replace(":0.0)",":0.000001)").replace(":0.0,",":0.000001,")

    print(treestr.split(";")[0])
    tree = Tree(treestr.split(";")[0]+";", format=1)
    print(tree)

    for node in tree.iter_descendants("levelorder"):
        #print(node.name)
        if not node.is_leaf():
            descendants=node.get_descendants()
            node_name=""
            for d in descendants:
                if d.is_leaf():
                     if len(node_name) < 1:
                         node_name=d.name
                     else:
                         node_name = node_name+"_"+d.name
            node.name = node_name

    with open(outfile,"w") as out:
        out.write(tree.write(format=1))

if __name__ == '__main__':
    main()
